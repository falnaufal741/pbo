/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sistem_pemesanan01;
import java.sql.*;
import java.sql.Connection;
import java.sql.SQLException;
/**
 *
 * @author User ID
 */
public class Koneksi {
 String driverdbms = "com.mysql.cj.jdbc.Driver";
    String database = "jdbc:mysql://localhost/pbouas";
    String user = "root";
    String password = "";
//    String database = "jdbc:mysql://192.168.10.253/a122106601";
//    String user = "a122106601";
//    String password = "polke001";
    
    public Koneksi()
    {}        
    
    public Connection getConnection() throws SQLException
    {
        Connection condbms = null;
        try
        {
            Class.forName(driverdbms);
            condbms = DriverManager.getConnection(database,user,password);
            
            return condbms;
        }
        catch (SQLException se)
        {
            System.out.println("Ada kesalahan pada SQL!");
            return null;
        } 
        catch(Exception ex)
        {
            System.out.println("Koneksi database tidak berhasil!");
            return null;
        }    
    }        
}

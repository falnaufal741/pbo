/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.praktikum04;

/**
 *
 * @author User ID
 */
import java.util.Scanner;
public class Praktikum04 {    
   
    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        
        // Input nama
        System.out.print("Masukkan nama: ");
        String nama = input.nextLine();
        
        // Input jurusan
        System.out.print("Masukkan jurusan: ");
        String jurusan = input.nextLine();
        
        // Input nilai
        System.out.print("Masukkan nilai angka: ");
        int nilai = input.nextInt();
        char nilaiHuruf;
        
        // Konversi nilai ke nilai huruf
        if(nilai >= 85 && nilai <= 100) {
            nilaiHuruf = 'A';
        } else if(nilai >= 70 && nilai < 85) {
            nilaiHuruf = 'B';
        } else if(nilai >= 60 && nilai < 70) {
            nilaiHuruf = 'C';
        } else if(nilai >= 50 && nilai < 60) {
            nilaiHuruf = 'D';
        } else if(nilai >= 0 && nilai < 50) {
            nilaiHuruf = 'E';
        } else {
            System.out.println("Nilai tidak valid.");
            return;
        }
        
        // Output hasil
        System.out.println("Hasil konversi:");
        System.out.println("Nama: " + nama);
        System.out.println("Jurusan: " + jurusan);
        System.out.println("Nilai angka: " + nilai);
        System.out.println("Nilai huruf: " + nilaiHuruf);
        
    }
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.praktikum04;

/**
 *
 * @author User ID
 */
import java.util.Scanner;
public class Praktikum04D {
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        // Input data dari user
        System.out.print("Masukan Nama: ");
        String Nama = input.nextLine();
        
        System.out.print("Masukan Nomor Pelanggan: ");
        String NomorPelanggan = input.nextLine();
        
        System.out.print("Masukan Pemakaian air: ");
        double PemakaianAir = input.nextDouble();
        
        // Hitung biaya pemakaian air
        double BiayaPemakaianAir;
        if (PemakaianAir <= 10) {
            BiayaPemakaianAir = PemakaianAir * 1000;
        } else if (PemakaianAir <= 20) {
            BiayaPemakaianAir = 10 * 1000 + (PemakaianAir - 10) * 2000;
        } else {
            BiayaPemakaianAir = 10 * 1000 + 10 * 2000 + (PemakaianAir - 20) * 5000;
        }
        
        // Tampilkan hasil
        System.out.println("Masukan Nama: " + Nama);
        System.out.println("Masukan Nomor Pelanggan: " + NomorPelanggan);
        System.out.println("Masukan Pemakaian Air: " + PemakaianAir);
        System.out.println("Masukan Biaya Pakai: " + BiayaPemakaianAir);
    }
}
